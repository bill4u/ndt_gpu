1. bugfix for Xavier NX: 
    1.1 deal nan elements in hessian matrix; 
    1.2 deal gpu 100% in buildParent() function;

This is a bug fix version of https://github.com/CPFL/Autoware/tree/master/ros/src/computing/perception/localization/lib/fast_pcl/ndt_gpu.

CUDA is required to build this program.
